#pragma once
#include "Node.h"
class stack
{
public:
	stack();
	Node* head;
	void push(Node*);
	Node* top();
	Node* pop();
};

