#pragma once
#include "Node.h"
class queue
{
public:
	queue();

	Node* tail;
	Node* head;
	
	void enqueue(Node*);
	Node* dequeue();
};

