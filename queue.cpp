#include "queue.h"
#include <iostream>
using namespace std;

queue::queue()
{
	head = nullptr;
	tail = nullptr;
}

void queue::enqueue(Node* node)
{
	Node* temp = head;
	cout << node->val << " enqueued\n";
	if (head == nullptr)
	{
		head = node;
		tail = node;
	}
	else
	{
		Node* temp = tail;
		tail = node;
		tail->next = temp;
	}

}

Node* queue::dequeue()
{
	Node* temp;
	Node* current;
	if (head == nullptr)
	{
		cout << "nothing on the Stack!" << "\n";
		return nullptr;
	}
	else if (head == tail)
	{
		temp = tail;
		head = nullptr;
		tail = nullptr;
		return temp;
	}
	else if (tail->next == head)
	{
		temp = head;
		head = tail;
		tail = head;
		head->next = nullptr;
		return temp;
	}
	else if (tail->next != nullptr)
	{
		current = tail;
		while (current->next != head)
		{
			current = current->next;
		}
		temp = head;
		head = current;
		head->next = nullptr;
		return temp;
	}
}
