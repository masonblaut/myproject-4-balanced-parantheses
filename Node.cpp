#include "Node.h"
#include <cstddef>

Node::Node()
{
	val = NULL;
	next = nullptr;
}

Node::Node(char c)
{
	val = c;
	next = nullptr;
}
