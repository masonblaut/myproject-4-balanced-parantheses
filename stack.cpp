#include "stack.h"
#include <iostream>
using namespace std;

stack::stack()
{
	head = nullptr;
}

void stack::push(Node* n)
{
	Node* temp = n;
	cout << temp->val << " pushed\n";
	if (head == nullptr)
	{
		head = temp;
	}
	else
	{
		temp->next = head;
		head = temp;
	}
}

Node* stack::top()
{
	if (head == nullptr)
	{
		cout << "Nothing is on the Stack";
	}
	else
	{
		return head;
	}
}

Node* stack::pop()
{
	Node* temp = new Node();
	if (head == nullptr)
	{
		cout << "Nothing is on the Stack";
	}
	else
	{
		temp = head;
		head = head->next;
		temp->next = nullptr;
		return temp;
		free(temp);
	}
}
