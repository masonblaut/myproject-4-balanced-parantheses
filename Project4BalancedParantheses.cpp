// Project4BalancedParantheses.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include "stack.h"
#include "queue.h"
#include "Node.h"
using namespace std;

int main()
{
	//Stack and Queue functionality tests

	stack theStack;
	queue theQueue;
	Node* node1 = new Node('{');
	Node* node2 = new Node('}');
	Node* node3 = new Node('[');
	Node* node4 = new Node(']');
	Node* node5 = new Node('5');

	theStack.push(node1);
	theStack.push(node3);


	theQueue.enqueue(node4);
	theQueue.enqueue(node2);

	cout << "the top of the stack should be [ ... it is " << theStack.top()->val << "\n";
	cout << "the top of the queue should be ] ... it is " << theQueue.head->val << "\n";
	cout << "\n";

	theStack.push(node5);
	cout << "theStack pop - " << theStack.pop()->val;
	cout << " new head = " << theStack.top()->val << "\n";
	cout << "theQueue dequeue - " << theQueue.dequeue()->val;
	cout << " new head = " << theQueue.head->val << "\n";

	// The Real Project begins
	cout << "\n";

	stack* charStack = new stack();
	queue* charQueue = new queue();

	char ch;
	fstream fin("Text.txt", fstream::in);
	while (fin >> noskipws >> ch) {
		if (ch == '{' || ch == '[' || ch == '(')
		{
			Node* temp = new Node();
			temp->val = ch;
			charStack->push(temp);
		}
		if (ch == '}' || ch == ']' || ch == ')')
		{
			Node* temp = new Node();
			temp->val = ch;
			charQueue->enqueue(temp);
		}
	}

	// Checking to see if both are the same
	if (charStack->top() != nullptr && charQueue->head != nullptr)
	{
		bool isBalanced = true;

		while (charStack->top() != nullptr && charQueue->head != nullptr && isBalanced!= false)
		{
			//cout << "\nanalysis started\n";
			char leftComp = charStack->pop()->val;
			cout << "\n"<< leftComp;
			char rightComp = charQueue->dequeue()->val;
			cout << " compares to " << rightComp << "\n";
			
			switch (leftComp)
			{
			case '{':
				if (rightComp != '}')
				{
					isBalanced = false;
				}
				break;
			case '[':
				if (rightComp != ']')
				{
					isBalanced = false;
				}
				break;
			case '(':
				if (rightComp != ')')
				{
					isBalanced = false;
				}
				break;
			}
			
		}

		if (isBalanced)
		{
			cout << "\nThe File read has balanced Parantheses!\n";
		}
		else
		{
			cout << "\nThe File read does not have balanced parantheses\n";
		}

	}
	else
	{
		cout << "nothing was on the stack or queue";
	}

    std::cout << "\n" << "END\n";
}

